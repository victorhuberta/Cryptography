from functools import wraps

def printfunc(func):
    """Print function's name when it's called for testing.

    Positional arguments:
    func -- wrapped function
        properties: function's name must start with 'test_'

    Returns: None
    """
    @wraps(func)
    def wrapper(self, *args):
        print('\n\nTesting "%s" function...' %
            func.__name__.split('test_')[1])
        return func(self, *args)

    return wrapper


def logcall(test, inputarg, expected,
        f, *args, should_raise=False, **kwargs):
    """Print input, expected output, and actual output of a tested function.

    Positional arguments:
    test -- unittest.TestCase object
    inputarg -- input argument to be printed (not necessarily argument to f)
    expected -- expected output from f
    f -- tested function
    *args -- list of positional arguments to f
    
    Keyword arguments:
    should_raise -- whether or not f is expected to raise an error
    **kwargs -- list of keyword arguments to f

    Returns: None
    """
    print('Input=%s; Expected=%s; ' % (inputarg, expected), end='')

    try:
        if should_raise:
            with test.assertRaises(expected):
                f(*args, **kwargs)
            result = expected.__name__
        else:
            result = f(*args, **kwargs)
            test.assertEqual(result, expected)
    except AssertionError as ae:
        print('TEST FAILED!')
        raise ae

    print('We got %s OK' % result)
