class Chain(object):
    """Represent a rainbow table chain."""

    def __init__(self, startpt, endpt, clen, dup, dist):
        """Create a new object of Chain.

        Positional arguments:
        startpt -- chain's starting plain text
        endpt -- chain's ending plain text
        clen -- chain's length
        dup -- chain's duplication level
            properties: dup >= 0.0 and dup <= 1.0
        dist -- chain's plain text lengths distribution
            properties: dist >= 0.0 and dist <= 1.0

        Returns:
        chain -- new object of Chain
        """
        self.startpt = startpt
        self.endpt = endpt
        self.clen = clen
        self.dup = dup
        self.dist = dist


    def __str__(self):
        """Create a string representation of this chain.

        Returns:
        chainstr -- chain's string representation
        """
        return \
            'Chain (%s -> %s)\n'\
            'Length: %d\n'\
            'Duplication: %f\n'\
            'Distribution: %s'\
                % (self.startpt, self.endpt,
                    self.clen, self.dup, self.dist)


class ChainMaker(object):
    """Make rainbow table chains."""

    def __init__(self, clen, algo, reductor):
        """Create a new object of ChainMaker.

        Positional arguments:
        clen -- chain's length
        algo -- hash algorithm to use
        reductor -- a Reductor object used for reduction

        Returns:
        cm -- new object of ChainMaker
        """
        self.clen = clen
        self.algo = algo
        self.reductor = reductor


    def hash(self, plaintext):
        """Produce the hash of plain text.

        Positional arguments:
        plaintext -- plain text to be hashed

        Returns:
        digest -- hashed plain text in string
        """
        return self.algo(plaintext.encode('utf-8')).hexdigest()


    def produce(self, startpt):
        """Produce a chain with a starting plain text.

        Each digest in the chain is reduced with a different
        reduce function, depending on its position at the time.
        Both plain text duplication and lengths distribution are
        recorded for each chain.

        Positional arguments:
        startpt -- starting plain text

        Returns:
        chain -- produced chain
        """
        plaintext = startpt
        plaintexts = [plaintext]

        # Original starting reduce function index.
        oristart = self.reductor.cur_rfno
        n_dups = 0

        for _ in range(self.clen):
            digest = self.hash(plaintext)
            plaintext = self.reductor.nextreduce(digest)

            if plaintext in plaintexts:
                n_dups += 1 # a duplicate found!
            plaintexts.append(plaintext)

        self.reductor.start_at(oristart) # Reset the starting index.

        return Chain(startpt, plaintext, self.clen,
            dup=self._calc_dup(n_dups, plaintexts),
            dist=self._calc_dist(plaintexts))


    def _calc_dup(self, n_dups, plaintexts):
        """Calculate the plain text duplication of a chain.

        Positional arguments:
        n_dups -- number of duplicates
        plaintexts -- all plain texts of a chain

        Returns:
        dup -- plain text duplication of chain
            properties: dup >= 0.0 and dup <= 1.0
        """
        return n_dups / len(plaintexts)


    def _calc_dist(self, plaintexts):
        """Calculate the plain text lengths distribution of a chain.

        Positional arguments:
        plaintexts -- all plain texts of a chain

        Returns:
        dist -- plain text lengths distribution of chain
        """
        dist = dict()

        # Count the number of plain texts with length n.
        for pt in plaintexts:
            n = len(pt)
            dist[n] = dist.get(n, 0) + 1

        total_count = sum(dist.values())
        for ptlen, count in dist.items():
            # Get distribution of plain text lengths.
            dist[ptlen] = count / total_count

        return dist
