import pickle
import gzip
from matplotlib import pyplot as plt

class RBTBuilder(object):
    """Build a rainbow table (NOT multi-processed).

    The internal rainbow table is represented as a dict. It will
    be pickled (Python object serialization) and stored as a
    compressed file.
    """

    def __init__(self, chain_maker):
        """Create a new object of RBTBuilder.

        Positional arguments:
        chain_maker -- ChainMaker object for producing chains

        Returns:
        builder -- new object of RBTBuilder
        """
        self.chain_maker = chain_maker
        self.rbtable = dict()


    def add_chain(self, startpt):
        """Create a new chain and add it into rainbow table.

        Positional arguments:
        startpt -- starting plain text for created chain

        Returns: None
        """
        chain = self.chain_maker.produce(startpt)
        self.rbtable[chain.endpt] = chain


    def build(self, ptlist):
        """Build a rainbow table from a list of starting plain texts.

        Positional arguments:
        ptlist -- list of starting plain texts

        Returns: None
        """
        self.rbtable = dict()

        for startpt in ptlist:
            self.add_chain(startpt)


    def build_fromfile(self, infile):
        """Build a rainbow table from a file with starting plain texts.

        Each starting plain text in infile must be separated by a newline.

        Positional arguments:
        infile -- input file name which contains the starting plain texts.

        Returns: None
        """
        self.rbtable = dict()

        with open(infile, 'r') as f:
            for line in f:
                self.add_chain(line.strip())


    def build_tofile(self, ptlist, outfile):
        """Build a rainbow table from ptlist and store it in outfile.

        Positional arguments:
        ptlist -- list of starting plain texts
        outfile -- output file name for rainbow table storage.

        Returns: None
        """
        self.rbtable = dict()

        for startpt in ptlist:
            self.add_chain(startpt)

        self.write_rbtable_tofile(outfile)


    def build_fromtofile(self, infile, outfile):
        """Build a rainbow table from infile and store it in outfile.

        Positional arguments:
        infile -- input file name which contains starting plain texts
        outfile -- output file name for rainbow table storage

        Returns: None
        """
        self.build_fromfile(infile)
        self.write_rbtable_tofile(outfile)


    def write_rbtable_tofile(self, outfile):
        """Write/store the rainbow table to outfile.
        
        The rainbow table is first being pickled, then it's stored
        in a compressed (GZIP) format.

        Positional arguments:
        outfile -- output file name for rainbow table storage

        Returns: None
        """
        pickled = pickle.dumps(self.rbtable)
        with gzip.open(outfile, 'wb') as f:
            f.write(pickled)
    

    def _report_mean_duplication(self):
        """Print the mean plain texts duplication of all produced chains.

        Returns:
        mean_dup -- mean plain texts duplication
        """
        total_dup = 0.0
        for _, chain in self.rbtable.items():
            total_dup += chain.dup

        mean_dup = total_dup / len(self.rbtable)
        print('Mean duplication for rainbow table: %f' % mean_dup)

        return mean_dup


    def _report_mean_distribution(self):
        """Print the mean plain text lengths distribution of all chains.

        Returns:
        mean_dist -- mean plain text lengths distribution
        """
        total_dist = dict()

        # Calculate total plain text lengths distribution.
        for _, chain in self.rbtable.items():
            for ptlen, dist_val in chain.dist.items():
                total_dist[ptlen] = total_dist.get(ptlen, 0) + dist_val

        mean_dist = dict()
        for ptlen, dist_total_val in total_dist.items():
            mean_dist[ptlen] = dist_total_val / len(self.rbtable)

        print('Mean distribution of plain text lengths '\
            'for rainbow table: %s' % mean_dist)
        
        return mean_dist


    def _dict_to_chart(self, d):
        """[HELPER] Take a dict and draw a corresponding bar chart.

        Positional arguments:
        d -- dict to draw the bar chart from

        Returns: None
        """
        d_range = range(len(d))

        plt.bar(d_range, d.values(), align='center')
        plt.xticks(d_range, list(d.keys()))
        plt.show()
