import hashlib
import time
import os

from lib.ptspec import AsciiPTSpec
from lib.chain import Chain, ChainMaker
from lib.reduction import Reductor
from lib.builder import RBTBuilder
from lib.attacker import RBTAttacker

seed = time.time()
spec = AsciiPTSpec(7, 7, alphabetic=True, numeric=True)
r = Reductor(seed, spec, hashlib.sha1, 5000)
cm = ChainMaker(5000, hashlib.sha1, r)
builder = RBTBuilder(cm)

pts = ['hello', 'world', 'what', 'are', 'you', 'doing', 'mate', 'shut',
        'bushido', 'as', 'no', 'x', '', 'wildcat']
builder.build_tofile(pts, 'examples.rbt')
builder._report_mean_duplication()
mean_dist = builder._report_mean_distribution()
builder._dict_to_chart(mean_dist)

os.unlink('examples.rbt')
