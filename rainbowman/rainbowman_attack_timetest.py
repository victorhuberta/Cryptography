import hashlib

from lib.ptspec import AsciiPTSpec
from lib.chain import Chain, ChainMaker
from lib.reduction import Reductor
from lib.attacker import RBTAttacker
from rainbowman_mp import mp_attack

target_hash = 'b7c274637ba7d9b75f68ff9de3bfacfada0d4469'

seed = 123456789.0
spec = AsciiPTSpec(7, 7, alphabetic=True, numeric=True)
r = Reductor(seed, spec, hashlib.sha1, 5000)
cm = ChainMaker(5000, hashlib.sha1, r)
attacker = RBTAttacker(cm, target_hash)
attacker.parse_file('examples.rbt')

plaintext = mp_attack(attacker)
print('Found: %s -> %s' % (plaintext, target_hash))
