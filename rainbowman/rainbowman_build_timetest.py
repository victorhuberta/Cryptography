import hashlib

from lib.ptspec import AsciiPTSpec
from lib.chain import Chain, ChainMaker
from lib.reduction import Reductor
from lib.builder import RBTBuilder
from rainbowman_mp import build_rbtable_fromtofile

seed = 123456789.0
spec = AsciiPTSpec(7, 7, alphabetic=True, numeric=True)
r = Reductor(seed, spec, hashlib.sha1, 5000)
cm = ChainMaker(5000, hashlib.sha1, r)
builder = RBTBuilder(cm)
# Run multi-processed version of build_fromtofile(...).
build_rbtable_fromtofile(builder, 'plaintexts.txt', 'examples.rbt')
