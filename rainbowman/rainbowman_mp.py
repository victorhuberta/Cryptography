import multiprocessing as mp

def build_rbtable_fromtofile(builder, infile, outfile):
    """Build a rainbow table from plain text file (multi-processed).

    Launch a number of processes. For every plain text read
    from infile, put it onto the plain texts queue. Processes will
    retrieve new plain texts from queue and create chains. Finished
    chains will be put onto the finished chains queue. Parent process
    then gather all finished chains to the builder object. Terminate
    all worker processes, and write rainbow table to file.

    Positional arguments:
    builder -- RBTBuilder object
    infile -- plain text file name
    outfile -- rainbow table file name

    Returns: None
    """
    n_plaintexts = 0
    n_workers = 10

    plaintexts = mp.Queue()
    finished_chains = mp.Queue()

    processes = launch_build_processes(n_workers, builder,
        plaintexts, finished_chains)

    with open(infile, 'r') as f:
        for line in f:
            n_plaintexts += 1
            # Put plain text into the queue to be processed.
            plaintexts.put(line.strip())

    gather_chains(builder, n_plaintexts, finished_chains)

    for process in processes: process.terminate()
    builder.write_rbtable_tofile(outfile)


def launch_build_processes(n_workers, builder, plaintexts, finished_chains):
    """Launch n_workers number of processes and hand them the queues.

    Positional arguments:
    n_workers -- number of worker processes
    builder -- RBTBuilder object to be used
    plaintexts -- plain texts queue
    finished_chains -- finished chains queue

    Returns:
    processes -- list of launched processes
    """
    processes = []

    for _ in range(n_workers):
        process = mp.Process(target=make_chains, args=
            (builder.chain_maker, plaintexts, finished_chains), daemon=True)
        process.start()
        processes.append(process)

    return processes


def gather_chains(builder, n_plaintexts, finished_chains):
    """Gather all finished chains to the builder object.

    Positional arguments:
    builder -- RBTBuilder object to be used as gathering point
    n_plaintexts -- total number of read plain texts
    finished_chains -- finished chains queue

    Returns: None
    """
    finished_count = 0

    while True:
        chain = finished_chains.get()
        builder.rbtable[chain.endpt] = chain
        finished_count += 1

        if finished_chains.empty():
            if finished_count >= n_plaintexts:
                # Only break after the count is verified.
                break


def make_chains(chain_maker, plaintexts, finished_chains):
    """Make chains from read plain texts and put them onto finished_chains.

    Positional arguments:
    chain_maker -- ChainMaker object to create chains
    plaintexts -- plain texts queue
    finished_chains -- finished chains queue

    Returns: None
    """
    while True:
        startpt = plaintexts.get()
        chain = chain_maker.produce(startpt)
        finished_chains.put(chain)


def mp_attack(attacker):
    """Attack a target hash (multi-processed).

    Get a list of jobs lists, and each list will be handed to a process.
    Launch a worker process for every list then hand it the attacker
    object, list of jobs, and "found chain" queue.
    
    Every process will try to find the matching chain and if found,
    put it onto the "found chain" queue. But if it fails, it puts None
    onto the "found chain" queue.
    
    Parent process will keep retrieving items from "found chain" queue
    until it finds a non-None value (which means a matching chain
    has been found). Parent process will try to find a matching plain
    text out of the found chain.

    Positional arguments:
    attacker -- RBTAttacker object

    Returns:
    plain text -- found plain text
        conditions: if plain text not found,
                    return None
    """
    jobs_list = get_jobs_list(attacker)

    # Count total number of jobs.
    n_jobs = sum([1 for jobs in jobs_list for _ in jobs])
    processes = []
    found_chain = mp.Queue()
                
    for jobs in jobs_list:
        process = mp.Process(target=find_chain_in_group,
            args=(attacker, jobs, found_chain), daemon=True)
        process.start()
        processes.append(process)

    finished_count = 0
    while finished_count < n_jobs:
        chain = found_chain.get()
        if chain == None:
            finished_count += 1
        else:
            break

    for process in processes: process.terminate()
    return attacker.find_plaintext(chain)


def get_jobs_list(attacker):
    """Get a list of jobs lists for worker processes.

    Create a list of lists with length n_workers. For each
    jobs list inside the list, append a starting chain position.
    The starting chain position index acts as a job representation.
    For example: start_chainpos of 34 means the process will start
    reducing plain texts from the 35th position in chain.

    All the jobs with similar workloads are spread evenly across
    worker processes.

    Positional arguments:
    attacker -- RBTAttacker object which has a reductor object

    Returns:
    jobs_list -- list of jobs lists
    """
    n_workers = 10
    reductor = attacker.chain_maker.reductor
    jobs_list = [[] for _ in range(n_workers)]

    start_chainpos_list = []
    for chainpos in range(attacker.max_clen):
        start_chainpos_list.append(chainpos)

        if len(start_chainpos_list) == n_workers:
            # Distribute jobs evenly across processes.
            assign_jobs(jobs_list, start_chainpos_list)
            start_chainpos_list = []

    # If total number of jobs isn't divisible by n_workers,
    # distribute the remaining N jobs to the first N workers.
    assign_jobs(jobs_list, start_chainpos_list)
    return jobs_list


def assign_jobs(jobs_list, start_chainpos_list):
    """Distribute jobs evenly across processes.

    Positional arguments:
    jobs_list -- list of jobs list
    start_chainpos_list -- list of starting chain position indexes

    Returns: None
    """
    for jobs, start_chainpos in zip(jobs_list, start_chainpos_list):
        jobs.append(start_chainpos)


def find_chain_in_group(attacker, jobs, found_chain):
    """Find matching chain for every given starting chain position index.

    Look at RBTAttacker.find_chain(...) for more information about how
    it actually does the search.

    Positional arguments:
    attacker -- RBTAttacker object
    jobs -- list of starting chain position indexes
    found_chain -- "found chain" queue to store job status

    Returns: None
    """
    for start_chainpos in jobs:
        chain = attacker.find_chain(start_chainpos)
        found_chain.put(chain)
