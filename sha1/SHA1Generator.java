import java.util.*;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.security.MessageDigest;

public class SHA1Generator {

    private static String strToHex(byte[] data) {
        StringBuffer buf = new StringBuffer();

        for (int i = 0; i < data.length; ++i) {
            int upperHalf = (data[i] >> 4) & 0x0F;
            int lowerHalf = data[i] & 0x0F;

            buf.append(byteToHex(upperHalf));
            buf.append(byteToHex(lowerHalf));
        }

        return buf.toString();
    }

    private static char byteToHex(int b) {
        return (char) ((b >= 0 && b <= 9) ? '0' + b : 'a' + (b - 10));
    }

    private static String sha1(String rawStr) throws NoSuchAlgorithmException,
        UnsupportedEncodingException {

        MessageDigest md = MessageDigest.getInstance("SHA-1");
        md.update(rawStr.getBytes("utf-8"), 0, rawStr.length());
        return strToHex(md.digest());
    }

    public static void main(String[] args) throws Exception { 
        Scanner input = new Scanner(System.in);
        String rawStr = input.nextLine();
        String hash = sha1(rawStr);
        System.out.println(hash);
    }

}
