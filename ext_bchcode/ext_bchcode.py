from bchcode.lib.bch import BCHCode

def spread_zeroes(_str, n_zeroes):
    """Spread zeroes throughout the encoded string.

    Positional Arguments:
    _str -- BCH encoded string
    n_zeroes -- number of zeroes to spread

    Returns:
    result -- list of padded encoded strings

    Examples:
    spread_zeroes('12', 2) == ['0012', '0102', '0120', '1002', '1020', '1200']

    spread_zeroes('12', 3) == ['00012', '01002', '01020', '01200', '00102',
        '00120', '10002', '10200', '10020', '12000']
    """
    if n_zeroes <= 0:
        return []

    N = len(_str)
    result = []

    for i in range(N+1):
        new_str = _str[:i] + ('0' * n_zeroes) + _str[i:]
        result.append(new_str)
        
        if i < N:
            for s in spread_zeroes(_str[i+1:], n_zeroes - 1):
                new_str = _str[:i] + '0' + _str[i:i+1] + s
                result.append(new_str)

            for j in range(n_zeroes - 1):
                for s in spread_zeroes(_str[i+1:], j):
                    new_str = _str[:i] + ('0' * (n_zeroes-j)) + _str[i:i+1] + s
                    result.append(new_str)

    return result


def spread_items(orig_a, n_items, *, item):
    if n_items == 0:
        return [orig_a]

    N = len(orig_a)
    result = []

    for i in range(N + 1):
        a = orig_a.copy()
        a.insert(i, item)

        for new_a in spread_items(a, n_items - 1, item=item):
            if new_a in result: continue
            result.append(new_a)

    return result


def main():
    bch = BCHCode(10, 6)
    encoded = input('Insert BCH encoded data: ')
    e_len = len(encoded)

    if e_len < 8:
        print('More than 2 missing pieces.')
        return

    result = spread_items(list(encoded), 10 - e_len, item='0')

    decoded_set = set()
    for padded in result:
        try:
            decoded = bch.decode(''.join(padded))
            decoded_set.add(decoded)
        except Exception as e:
            continue
    
    if len(decoded_set) == 0:
        print('Couldn\'t be corrected!')
    else:
        for decoded in decoded_set:
            print('Possible correction: %s' % decoded)


if __name__ == '__main__':
    main()
