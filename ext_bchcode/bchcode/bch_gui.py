from tkinter import *
from tkinter import ttk
from tkinter import messagebox
from lib.bch import BCHCode

bchcode = BCHCode(10, 6)

def encode_numbers(numbers, result):
    """Encode a string of numbers taken from an entry.

    Display an error dialog if an exception is thrown.
    
    Positional arguments:
    numbers -- user provided numbers (UI text var) 
    result -- variable to store encoded numbers (UI text var)

    Returns: None
    """
    try:
        result.set(bchcode.encode(numbers.get()))
    except Exception as err:
        messagebox.showerror(message=str(err))


def correct_numbers(numbers, result):
    """Decode/correct a string of numbers taken from an entry.

    Display an error dialog if an exception is thrown.

    Positional arguments:
    numbers -- user provided numbers (UI text var) 
    result -- variable to store encoded numbers (UI text var)

    Returns: None
    """
    try:
        result.set(bchcode.decode(numbers.get()))
    except Exception as err:
        messagebox.showerror(message=str(err))


def main():
    root = Tk()
    root.title('BCH(10, 6) Tool')
    
    mainframe = ttk.Frame(root, padding='3 3 12 12')
    mainframe.grid(row=0, column=0, sticky=(N, E, S, W))
    mainframe.rowconfigure(0, weight=1)
    mainframe.columnconfigure(0, weight=1)

    # Text variables bound to widgets
    numbers = StringVar()
    result = StringVar()

    # Numbers label and entry
    ttk.Label(mainframe, text='Numbers').grid(row=0, column=0, sticky=(W, E))
    numbers_entry = ttk.Entry(mainframe, width=12, textvariable=numbers)
    numbers_entry.grid(row=0, column=1, sticky=(W, E), columnspan=2)
    numbers_entry.focus()

    # Encode and decode/correct buttons
    encode_button = ttk.Button(mainframe, text='Encode',
        command=lambda : encode_numbers(numbers, result))
    encode_button.grid(row=1, column=1, sticky=W)

    correct_button = ttk.Button(mainframe, text='Correct',
        command=lambda : correct_numbers(numbers, result))
    correct_button.grid(row=1, column=2, sticky=E)

    # Result label and entry
    ttk.Label(mainframe, text='Result').grid(row=2, column=0, sticky=(W, E))
    result_entry = ttk.Entry(mainframe, width=12,
        textvariable=result, state='readonly')
    result_entry.grid(row=2, column=1, sticky=(W, E), columnspan=2)

    # Configure padding for all mainframe's children.
    for child in mainframe.winfo_children():
        child.grid_configure(padx=5, pady=5)

    root.mainloop()


if __name__ == '__main__':
    main()
