from abc import ABCMeta, abstractmethod
from .bcherrors import *
from .bchutils import transpose_matrix, inverse, sqrtmod
from .gmatrix import GMatrixSolver

class Code(metaclass=ABCMeta):

    @abstractmethod
    def encode(self, s): pass

    
    @abstractmethod
    def decode(self, e): pass


class BCHCode(Code):
    """BCH encode and decode string of numbers.

    To learn more about BCH code, visit:
    https://en.wikipedia.org/wiki/BCH_code
    """

    def __init__(self, out_len, in_len):
        """Create a new BCHCode object.

        Positional arguments:
        out_len -- output length after encoding
            properties: out_len > in_len
        in_len -- input length before encoding
            properties: in_len < out_len

        Returns:
        BCHCode object

        Examples:
        BCHCode(10, 6)'s encode() accepts '123456'
        as input and returns '123456XXXX' as output.
        """
        if out_len <= in_len:
            raise InvalidBCHCode('Output length <= input length')

        self.out_len = out_len
        self.in_len = in_len
        self.boundary = self.out_len + 1
        self.pchkmatrix = self._gen_pchkmatrix()
        self.gmatrix = self._gen_genmatrix(self.pchkmatrix)


    def encode(self, s):
        """BCH encode a string of numbers.

        Positional arguments:
        s -- a string of numbers
            properties: len(s) == self.in_len

        Returns:
        e -- a BCH encoded string of numbers
            properties: len(e) == self.out_len

        Examples:
        b = BCHCode(10, 6)
        b.encode('123345') == '1233458396'
        b.encode('456789') == '4567897153'
        """
        matrix = transpose_matrix(self.gmatrix)
        digits = [int(c) for c in s]
        new_digits = self._mult_matrix_digits(matrix, digits)

        for new_digit in new_digits:
            if new_digit >= self.out_len:
                self._raise_unusable_number_err(new_digit)

        return ''.join([str(d) for d in new_digits])


    def decode(self, e):
        """BCH decode (correct) a BCH encoded string of numbers.

        Positional arguments:
        e -- a BCH encoded string of numbers
          properties: len(e) == self.out_len

        Returns:
        d -- a BCH decoded (corrected) string of numbers

        Examples:
        b = BCHCode(10, 6)
        e = b.encode('123345')
        # e == '1233458396'; then e was modified to '1234458396'
        d = b.decode(e)
        # d == '1233458396'; d is the corrected version of e
        """
        matrix = self.pchkmatrix
        digits = [int(c) for c in e]

        # Get syndrome numbers.
        s = self._mult_matrix_digits(matrix, digits)

        # If all syndrome numbers are zeroes, there's no error.
        if list(filter(lambda x: x != 0, s)) == []:
            return e

        # p, q, r are determinants for single error / double errors.
        p = (s[1]**2 - s[0]*s[2]) % self.boundary
        q = (s[0]*s[3] - s[1]*s[2]) % self.boundary
        r = (s[2]**2 - s[1]*s[3]) % self.boundary

        if p == 0 and q == 0 and r == 0:
            return self._single_error_correction(e, s)
        else:
            return self._double_errors_correction(e, s, p, q, r)
            

    def _mult_matrix_digits(self, matrix, digits):
        """Multiply a list of digits with a matrix.

        Positional arguments:
        matrix -- generator / parity check matrix for this object
        digits -- a list of integers

        Returns:
        new_digits -- a new list of integers
            new_digits is a list of syndrome numbers if matrix is
            parity check matrix.
        """
        new_digits = []

        for coefficients in matrix:
            # Multiply each coefficient (taken from matrix)
            # with each digit.
            new_digit = sum([pair[0] * pair[1] for pair in
                zip(coefficients, digits)]) % self.boundary
            new_digits.append(new_digit)

        return new_digits


    def _single_error_correction(self, e, s):
        """Correct single digit error in a BCH encoded string of numbers.

        Positional arguments:
        e -- a BCH encoded string of numbers
        s -- a list of syndrome numbers

        Returns:
        fixed -- the corrected version of e

        Raises:
        NoPossibleCorrectionError
        """
        # Error magnitude
        err_mag = s[0] % self.boundary
        # Error position
        err_pos = s[1] * inverse(err_mag, mod=self.boundary) % self.boundary

        if err_pos == 0:
            # Error position starts from 1, 0 means more than 2 errors.
            self._raise_no_possible_correction_err()

        return self._correct_str(e, err_pos, err_mag)


    def _double_errors_correction(self, e, s, p, q, r):
        """Correct multiple digits error in a BCH encoded string of numbers.

        Positional arguments:
        e -- a BCH encoded string of numbers
        s -- a list of syndrome numbers
        p, q, r -- determinants for single error / double errors.

        Returns:
        fixed -- the corrected version of e

        Raises:
        NoPossibleCorrectionError
        """
        try:
            # Just a part of a bigger calculation.
            part = sqrtmod((q**2 - 4*p*r) % self.boundary, mod=self.boundary)

            # Calculate first and second error positions.
            err_pos1 = (-q + part) * inverse(2*p,
                mod=self.boundary) % self.boundary
            err_pos2 = (-q - part) * inverse(2*p,
                mod=self.boundary) % self.boundary

        except (NoSquareRootModuloError, NoInverseError) as err:
            self._raise_no_possible_correction_err()

        if err_pos1 == 0 or err_pos2 == 0:
            # Error position starts from 1, 0 means more than 2 errors.
            self._raise_no_possible_correction_err()

        # Calculate first and second error magnitudes.
        err_mag2 = ((err_pos1*s[0] - s[1]) * \
            inverse(err_pos1 - err_pos2, mod=self.boundary)) % self.boundary
        err_mag1 = (s[0] - err_mag2) % self.boundary

        try:
            # Correct the string twice because of 2 errors.
            fixed = self._correct_str(e, err_pos1, err_mag1)
            fixed = self._correct_str(fixed, err_pos2, err_mag2)
        except UnusableNumberError as err:
            self._raise_no_possible_correction_err()

        return fixed

    

    def _correct_str(self, e, err_pos, err_mag):
        """Replace a digit in a string of numbers.

        Positional arguments:
        e -- a BCH encoded string of numbers
        err_pos -- error position (starts from 1)
        err_mag -- error magnitude

        Returns:
        fixed -- a "fixed" string of numbers

        Raises:
        UnusableNumberError
        """
        fixed = list(e[:]) # make a copy
        replacement = (int(fixed[err_pos - 1]) - err_mag) % self.boundary

        if replacement >= self.out_len:
            # out_len and above are not allowed for a digit.
            self._raise_unusable_number_err(replacement)

        fixed[err_pos - 1] = str(replacement)
        return ''.join(fixed)
    

    def _gen_pchkmatrix(self):
        """Generate a parity check matrix for BCH code.

        Returns:
        matrix -- a parity check matrix
            it's represented in a multi-dimensional array
        """
        matrix = []
        n_extradigits = self.out_len - self.in_len

        for i in range(n_extradigits):
            matrix.append([(digit ** i) % self.boundary
                for digit in range(1, self.boundary)])

        return matrix


    def _gen_genmatrix(self, pchkmatrix):
        """Generate a generator matrix for BCH code.

        Positional arguments:
        pchkmatrix -- generated parity check matrix for this BCH code

        Returns:
        gmatrix -- a generator matrix
            it's represented in a multi-dimensional array
        """
        solver = GMatrixSolver(self.out_len, self.in_len, pchkmatrix)
        return solver.solve()


    def _raise_unusable_number_err(self, number):
        raise UnusableNumberError('The number %d is unusable' % number)


    def _raise_no_possible_correction_err(self):
        raise NoPossibleCorrectionError('More than 2 errors.')
