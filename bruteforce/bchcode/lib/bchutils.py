from fractions import Fraction
from math import gcd
from .bcherrors import *

def transpose_matrix(matrix):
    """Transpose a matrix.

    Positional arguments:
    matrix -- a multi-dimensional array

    Returns:
    tr_matrix -- transposed matrix

    Examples:
    transpose_matrix(*[[1, 2, 3], [4, 5, 6]]) == [[1, 4], [2, 5], [3, 6]]
    """
    return list(map(list, zip(*matrix)))


def inverse(n, *, mod):
    """Find a modular inverse of n.

    Positional arguments:
    n -- integer which might (not) have a modular inverse

    Keyword arguments:
    mod -- base value to % with

    Returns:
    i -- modular inverse of n

    Raises:
    NoInverseError
    """
    for i in range(mod):
        if (n * i) % mod == 1:
            return i
    raise NoInverseError('There\'s no inverse for ' + str(n))


def sqrtmod(n, *, mod):
    """Find the modular square root of n.

    Positional arguments:
    n -- integer which might (not) have a modular square root

    Keyword arguments:
    mod -- base value to % with

    Returns:
    sr -- modular square root of n

    Raises:
    NoSquareRootModuloError
    """
    for sr in range(mod):
        if (sr ** 2) % mod == n:
            return sr
    raise NoSquareRootModuloError(
        'There\'s no square root modulo for ' + str(n))


def print_matrix(m):
    """Print a MxN matrix.

    Positional arguments:
    m -- matrix to be printed

    Returns: None
    """
    m_str = ''
    for row in m:
        for col in row:
            m_str += ('%-8s'% col)
        m_str += '\n'
    print(m_str)


def _lv_arith(op, l, v, *, fraction=True, mod=None):
    """Perform an arithmetic operation on a list and a value.
    
    This function is just a wrapper for _ll_arith(...).
    Missing information can be found there.

    Positional arguments:
    v -- value to be operated on
    """
    return _ll_arith(op, l, [v] * len(l), fraction=fraction, mod=mod)


def _ll_arith(op, l1, l2, *, fraction=True, mod=None):
    """Perform an arithmetic operation on two lists.

    Note: this function is specialized to handle fractional
    arithmetic operations.

    Positional arguments:
    op -- arithmetic operator function
    l1 -- first list to be operated on
    l2 -- second list to be operated on

    Keyword arguments:
    fraction -- whether or not to treat numbers as fractions (default True)
    mod -- arithmetic modulus (default None)

    Return:
    nl -- new list after arithmetic operation
    """
    if mod != None:
        if fraction:
            return [frmod(op(v1, v2), mod=mod) for v1, v2 in zip(l1, l2)]
        else:
            return [op(v1, v2) % mod for v1, v2 in zip(l1, l2)]
    else:
        return [op(v1, v2) for v1, v2 in zip(l1, l2)]


def frmod(n, *, mod):
    """Perform modular operation on a fraction number.

    Positional arguments:
    n -- fraction to be operated on
        properties: gcd(n.denominator, mod) == 1

    Keyword arguments:
    mod -- arithmetic modulus
        properties: is_prime(mod) == True

    Raises:
    FractionModuloError

    Returns:
    fr -- fraction from new numerator and new denominator
    """
    nom, denom = n.numerator, n.denominator
    if gcd(denom, mod) != 1:
        raise FractionModuloError('Denominator and modulus are NOT coprimes')

    while denom != 1:
        x = 2
        while True:
            if gcd(x, mod) == 1:
                if denom * x >= mod:
                    break
            x += 1
        nom *= x; denom *= x
        nom %= mod; denom %= mod

    if nom < 0: nom %= mod
    return Fraction(nom, denom)
