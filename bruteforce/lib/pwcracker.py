import multiprocessing as mp
import itertools

# Queue to store all tested plain texts and digests.
# Used by the module's GUI.
tested_result = mp.Queue()

# Queue to store cracking result from each process.
finish = mp.Queue()

def mp_crack(target_hash, algo, ptg):
    """Multi-processed cracking of target_hash.

    Each plain text list generated will be handed
    to a separate process. Each process will then
    try to find a matching hash.

    Positional arguments:
    target_hash -- target hash to crack
    algo -- hash algorithm to use
    ptg -- plain text generator to use

    Returns:
    result -- (plain text, digest(plain text))
    """
    processes = []

    for ptlist in ptg.generate():
        # Create a process for every plain text list.
        process = mp.Process(target=find_pass,
            args=(target_hash, algo, ptlist))
        process.daemon = True
        process.start()
        processes.append(process)

    n_failedprocs = 0
    result = None

    while n_failedprocs < len(processes):
        # Get "job finished" status from processes.
        result = finish.get()
        if result != None: break

        # result == None means no match has yet been found.
        n_failedprocs += 1

    for process in processes: process.terminate()
    return result


def find_pass(target_hash, algo, ptlist):
    """Find matching password/plain text from a list of plain texts.

    If a matching password is found, put (plain text, digest(plain text))
    into the finish queue. If no match found after iterating all plain texts,
    put None into the finish queue.

    Positional arguments:
    target_hash -- target hash to crack
    algo -- hash algorithm to use
    ptlist -- plain text list

    Returns: None
    """
    for pt in ptlist:
        digest = algo(pt.encode('utf-8')).hexdigest()
        result = (pt, digest)

        # tested results are displayed by the GUI on screen.
        tested_result.put(result)

        if digest == target_hash:
            finish.put(result)

    finish.put(None)
