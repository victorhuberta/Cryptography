from tkinter import *
from tkinter import ttk
from tkinter import messagebox
import hashlib
import threading
import multiprocessing as mp
import time

from lib.ptspec import AsciiPTSpec
from lib.ptgenerator import PrintablePTGenerator, BCH106PTGenerator
from lib.pwcracker import mp_crack, tested_result

root = Tk()
root.title('Password Cracker')

target_hash = StringVar()
algorithm = StringVar()
initlen = IntVar()
maxlen = IntVar()
is_alphabetic = BooleanVar()
is_numeric = BooleanVar()
is_bchcode = BooleanVar()
n_plaintexts = StringVar()
report = StringVar()

def validate_and_crack():
    """Perform validation and start cracking of target hash."""

    if target_hash.get() == '':
        messagebox.showerror(message='Target hash can\'t be empty!')
        return

    if initlen.get() > maxlen.get():
        messagebox.showerror(message='Min length must be larger than '\
            'or equal to max length.')
        return

    if not is_alphabetic.get() and not is_numeric.get() and \
        not is_bchcode.get():
        messagebox.showerror(message='Select at least one criterion.')
        return

    crack()


def crack():
    """Crack target hash by using plain texts generator and mp_crack()."""

    # Validate chosen hash algorithm.
    if algorithm.get() == 'SHA-1':
        algo = hashlib.sha1
    elif algorithm.get() == 'MD5':
        algo = hashlib.md5
    else:
        messagebox.showerror(message='Selected hash algorithm not supported.')
        return

    try:
        # Prepare a plain text generator object.
        if is_bchcode.get():
            # Wrap generator object with BCH106PTGenerator when necessary.
            ptspec = AsciiPTSpec(6, 6, numeric=True)
            ptg = BCH106PTGenerator(PrintablePTGenerator(ptspec))
        else:
            ptspec = AsciiPTSpec(initlen.get(), maxlen.get(),
                alphabetic=is_alphabetic.get(), numeric=is_numeric.get())
            ptg = PrintablePTGenerator(ptspec)

        # Calculate the total number of possible plain texts.
        ptspace = 0
        for ptlen in range(ptspec.get_minlen(), ptspec.get_maxlen() + 1):
            ptspace += (len(ptspec.get_charset()) ** ptlen)

        # Update GUI with the calculation result.
        n_plaintexts.set('Total possible plain texts: ' + str(ptspace))

        # Start a new thread (so GUI doesn't block) for cracking.
        threading.Thread(target=crack_and_update_ui,
            args=(target_hash.get(), algo, ptg), daemon=True).start()

        disable_widgets()
    except Exception as err:
        messagebox.showerror(message=str(err))


def crack_and_update_ui(*args):
    """Perform multi-processed cracking while also updating GUI.

    Positional arguments:
    *args -- all arguments that will be passed to mp_crack()

    Returns: None
    """
    threading.Thread(target=update_tested_results, daemon=True).start()
    result = mp_crack(*args)

    # Put None to stop update_tested_results() thread from updating GUI.
    tested_result.put(None)

    # Update GUI with result.
    while True:
        time.sleep(0.5) # To compensate for update timing
        if tested_result.empty():
            if result != None:
                report.set('Found :) Plain text is \'%s\'.' % result[0])
            else:
                report.set('Plain text not found :(')
            return


def update_tested_results():
    """Update GUI with tested plain texts and their digests."""
    while True:
        tested = tested_result.get()
        if tested == None:
            return
        report.set('Matching %10s -> %32s...' % (tested[0], tested[1]))


def disable_widgets():
    """Disable a list of widgets to prevent further user interaction."""
    widgets = [hash_entry, algo_cmbbox, initlen_spin, maxlen_spin,
        alphabets_chkbtn, numbers_chkbtn, bchcode_chkbtn, crack_button]

    for widget in widgets:
        widget.config(state='disabled')


def disable_chkbtns():
    """Disable other check buttons when is_bchcode is checked."""
    if is_bchcode.get():
        is_alphabetic.set(False)
        alphabets_chkbtn.config(state='disabled')

        is_numeric.set(True)
        numbers_chkbtn.config(state='disabled')
    else:
        alphabets_chkbtn.config(state='normal')
        numbers_chkbtn.config(state='normal')


# Setting up the mainframe
mainframe = ttk.Frame(root, padding='3 3 12 12')
mainframe.grid(row=0, column=0, sticky=(N, W, E, S))
mainframe.rowconfigure(0, weight=1)
mainframe.columnconfigure(0, weight=1)

# Target hash label and text box
ttk.Label(mainframe, text='Target Hash').grid(
    row=0, column=0, sticky=(W, E))
hash_entry = ttk.Entry(mainframe, width=48, textvariable=target_hash)
hash_entry.grid(row=0, column=1, sticky=(W, E), columnspan=4)

# Algorithm label and combo box
ttk.Label(mainframe, text='Algorithm').grid(row=1, column=0, sticky=(W, E))
algo_cmbbox = ttk.Combobox(mainframe, textvariable=algorithm,
    width=10, state='readonly')
algo_cmbbox['values'] = ('SHA-1', 'MD5') # only support these algorithms
algo_cmbbox.current(0)
algo_cmbbox.grid(row=1, column=1, sticky=(W, E))

# Min length and max length spin boxes
ttk.Label(mainframe, text='Min').grid(row=1, column=2, sticky=E)
initlen_spin = Spinbox(mainframe, textvariable=initlen,
    width=3, from_=0, to=32, increment=1)
initlen_spin.grid(row=1, column=3, sticky=(W, E))

ttk.Label(mainframe, text='Max').grid(row=2, column=2, sticky=E)
maxlen_spin = Spinbox(mainframe, textvariable=maxlen,
    width=3, from_=0, to=32, increment=1)
maxlen_spin.grid(row=2, column=3, sticky=(W, E))

# Alphabets, numbers, and BCHCode(10, 6) check buttons
alphabets_chkbtn = ttk.Checkbutton(mainframe, text='Alphabets',
    variable=is_alphabetic, onvalue=True, offvalue=False)
alphabets_chkbtn.grid(row=3, column=1, sticky=W)

numbers_chkbtn = ttk.Checkbutton(mainframe, text='Numbers',
    variable=is_numeric, onvalue=True, offvalue=False)
numbers_chkbtn.grid(row=3, column=2, sticky=(W, E))

bchcode_chkbtn = ttk.Checkbutton(mainframe, text='BCHCode(10, 6)',
    command=disable_chkbtns, variable=is_bchcode, onvalue=True, offvalue=False)
bchcode_chkbtn.grid(row=3, column=3, sticky=W, columnspan=2)

# Crack button
crack_button = ttk.Button(mainframe, text='Crack', command=validate_and_crack)
crack_button.grid(row=4, column=1, sticky=(W, E))

# Total possible plaintexts label
ttk.Label(mainframe, textvariable=n_plaintexts).grid(row=5, column=1,
    sticky=(W, E), columnspan=2)

# Cracking report
ttk.Label(mainframe, textvariable=report).grid(row=6, column=1,
    sticky=(W, E), columnspan=5)

# Add paddings for all children of mainframe.
for child in mainframe.winfo_children():
    child.grid_configure(padx=5, pady=5)

root.mainloop()
