import hashlib
from lib.pwcracker import mp_crack
from lib.ptspec import AsciiPTSpec
from lib.ptgenerator import PrintablePTGenerator

# IMPORTANT: before running this, remember to comment out the
# tested_result.put(result) in lib.pwcracker.find_pass(...) function.
# This is because without the GUI, nothing will take stuff out
# of the shared queue, causing RAM overload.

spec = AsciiPTSpec(0, 10, alphabetic=False, numeric=True)
ptg = PrintablePTGenerator(spec)
target_hash = 'b6589fc6ab0dc82cf12099d1c2d40ab994e8410c'
algo = hashlib.sha1

result = mp_crack(target_hash, algo, ptg)
print('Found: %s -> %s' % (result[0], result[1]))
